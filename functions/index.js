const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

const db = admin.firestore();
const firebaseConfig = functions.config();
const stripe = require('stripe')(firebaseConfig.stripe.testkey);

// Create Stripe Customer
// this uses a collections called 'Users' which is generated alongside Firebase auth
// If only using Firbase auth using something like:
// exports.createStripeUser = functions.auth.user().onCreate((user) => {
exports.createStripeUser = functions.firestore.document('Users/{userId}').onCreate((snap, context) => {
    const user = snap.data();

    // register Stripe user
    return stripe.customers.create({
        email: user.email,
        name: user.name
    })
    .then(customer => {

        /// update database with stripe customer id
        const data = {
            stripe_id: customer.id,
        }
        return db.collection('Users').doc(context.params.userId).update(data);
    });
});
